import java.util.Properties
import org.jetbrains.dokka.gradle.DokkaTask

val projectSettings = fetchProjectSettings()

group = "org.digieng"
version = if (projectSettings.isDevVer) "${projectSettings.libVer}-dev" else projectSettings.libVer

plugins {
    kotlin("multiplatform") version "1.4.31"
    id("maven-publish")
    id("org.jetbrains.dokka") version "1.4.32"
    signing
}

repositories {
    mavenCentral()
}

val dokkaHtml by tasks.getting(DokkaTask::class)

val javadocJar by tasks.register("javadocJar", Jar::class) {
    dependsOn(dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaHtml.outputDirectory)
}

signing {
    useGpgCmd()
    sign(configurations.archives.get())
}

val dokkaJar by tasks.creating(Jar::class.java) {
    archiveClassifier.set("javadoc")
    from(tasks.dokkaHtml)
}

kotlin {
    linuxX64("linuxX64") {
        compilations.getByName("main") {
            cinterops.create("paho_mqtt") {
                val pahoMqttDir = "${System.getenv("HOME")}/paho_mqtt_c-1.3.7"
                val includeDir = "$pahoMqttDir/include"
                compilerOpts(includeDir)
                extraOpts("-libraryPath", "$pahoMqttDir/lib")
                includeDirs(includeDir)
            }
        }
    }
    jvm("jvm") {
        compilations.getByName("main") {
            dependencies {
                val pahoClientVer = "1.2.0"
                implementation("org.eclipse.paho:org.eclipse.paho.client.mqttv3:$pahoClientVer")
                implementation(kotlin("stdlib-jdk8"))
            }
        }
    }

    sourceSets {
        commonMain {
            dependencies {
                val kotlinVer = "1.4.31"
                implementation(kotlin("stdlib-common", kotlinVer))
            }
        }
    }

    publishing {
        publications.withType<MavenPublication> {
            if (projectSettings.includeDocs) artifact(javadocJar)
            createPom()
        }
    }
}

fun MavenPublication.createPom() = pom {
    name.set("KMQTT Client")
    description.set("A MQTT client library for Kotlin JVM, and Kotlin Native based projects.")
    licenses {
        license {
            name.set("The Apache License, Version 2.0")
            url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
        }
    }
    developers {
        developer {
            id.set("napperley")
            name.set("Nick Apperley")
            email.set("napperley@protonmail.com")
        }
    }
    scm {
        connection.set("scm:git:git:https://gitlab.com/napperley/kmqtt-client.git")
        developerConnection.set("scm:git:ssh:git@gitlab.com:napperley/kmqtt-client.git")
    }
}

val Boolean.intValue: Int
    get() = if (this) 1 else 0

data class ProjectSettings(val libVer: String, val isDevVer: Boolean, val includeDocs: Boolean)

fun fetchProjectSettings(): ProjectSettings {
    var libVer = "SNAPSHOT"
    var isDevVer = true
    var includeDocs = false
    val properties = Properties()
    file("project.properties").inputStream().use { inputStream ->
        properties.load(inputStream)
        libVer = properties.getProperty("libVer") ?: "SNAPSHOT"
        @Suppress("RemoveSingleExpressionStringTemplate")
        isDevVer = "${properties.getProperty("isDevVer")}".toBoolean()
        @Suppress("RemoveSingleExpressionStringTemplate")
        includeDocs = "${properties.getProperty("includeDocs")}".toBoolean()
    }
    return ProjectSettings(libVer = libVer, isDevVer = isDevVer, includeDocs = includeDocs)
}
