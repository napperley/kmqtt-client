package org.digieng.kmqtt.client

/** Type of persistence to use for MQTT. */
object MqttPersistence {
    const val NONE = 0
    const val FILE = 1
    const val MEMORY = 2
}
