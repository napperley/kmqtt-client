package org.digieng.kmqtt.client

import paho_mqtt.MQTTCLIENT_PERSISTENCE_DEFAULT
import paho_mqtt.MQTTCLIENT_PERSISTENCE_ERROR
import paho_mqtt.MQTTCLIENT_PERSISTENCE_NONE
import paho_mqtt.MQTTCLIENT_PERSISTENCE_USER

/** Provides all MQTT persistence types. */
@Suppress("unused")
object MqttPersistence {
    const val NONE = MQTTCLIENT_PERSISTENCE_NONE
    const val DEFAULT = MQTTCLIENT_PERSISTENCE_DEFAULT
    const val ERROR = MQTTCLIENT_PERSISTENCE_ERROR
    const val USER = MQTTCLIENT_PERSISTENCE_USER
}
