@file:Suppress("EXPERIMENTAL_UNSIGNED_LITERALS", "EXPERIMENTAL_API_USAGE")

package org.digieng.kmqtt.client

import kotlinx.cinterop.*
import paho_mqtt.*
import platform.posix.free
import kotlin.native.concurrent.freeze

@Suppress("unused")
actual class MqttClient(
    @Suppress("MemberVisibilityCanBePrivate") actual val brokerUrl: String,
    @Suppress("MemberVisibilityCanBePrivate") actual val clientId: String = "Default",
    @Suppress("MemberVisibilityCanBePrivate") val persistenceType: Int = MqttPersistence.NONE,
    deliveryComplete: (Int) -> Unit = {},
    connectionLost: (String) -> Unit = {},
    messageArrived: (String, MqttMessage) -> Unit = { _, _ -> }
) {
    private val evtHandlers = MqttEventHandlers(
        deliveryComplete = deliveryComplete,
        connectionLost = connectionLost,
        messageArrived = messageArrived
    ).freeze()
    private val stableRef = StableRef.create(evtHandlers)
    private val arena = Arena()
    private val client = arena.alloc<MQTTClientVar>()
    private val connOptionsPtr = createMqttConnectionOptions()
    private var _isOpen = true

    /** If *true* then the [MqttClient] instance can be used, otherwise a new instance will need to be created. */
    val isOpen: Boolean
        get() = _isOpen
    actual val isConnected: Boolean
        get() = isOpen && MQTTClient_isConnected(client.value) == true.toInt()
    actual var deliveryCompleteHandler: (Int) -> Unit = {}
    actual var connectionLostHandler: (String) -> Unit = {}
    actual var messageArrivedHandler: (String, MqttMessage) -> Unit = { _, _ -> }

    init {
        MQTTClient_create(
            handle = client.ptr,
            serverURI = brokerUrl,
            clientId = clientId,
            persistence_type = persistenceType,
            persistence_context = null
        )
        updateCallbacks()
    }

    private fun updateCallbacks() {
        if (isOpen && !isConnected) {
            MQTTClient_setCallbacks(
                handle = client.value,
                context = stableRef.asCPointer(),
                ma = staticCFunction(::onMessageArrived),
                cl = staticCFunction(::onConnectionLost),
                dc = staticCFunction(::onDeliveryComplete)
            )
        }
    }

    @Suppress("unused", "RedundantSuspendModifier")
    actual suspend fun publish(topic: String, msg: MqttMessage, timeout: Long): MqttError? = memScoped {
        val deliveryToken = alloc<MQTTClient_deliveryTokenVar>()
        var result: MqttError? = null
        if (isOpen && isConnected) {
            MQTTClient_publish(
                handle = client.value,
                topicName = topic,
                payload = msg.payload.cstr,
                payloadlen = msg.payload.length,
                retained = if (msg.retained) true.toInt() else false.toInt(),
                qos = msg.qos.value,
                dt = deliveryToken.ptr
            )
            result = handlePublishCompletion(deliveryToken.value, timeout.toULong())
        }
        return result
    }

    private fun handlePublishCompletion(deliveryToken: MQTTClient_deliveryToken, timeout: ULong): MqttError? {
        var result: MqttError? = null
        val rc = MQTTClient_waitForCompletion(
            handle = client.value,
            dt = deliveryToken,
            timeout = timeout
        )
        if (rc != MQTTCLIENT_SUCCESS) {
            result = MqttError("Cannot publish MQTT message (code $rc).", MqttStatus.MSG_DELIVERY_FAILED)
        }
        return result
    }

    @Suppress("unused", "RedundantSuspendModifier")
    actual suspend fun subscribe(topic: String, qos: MqttQos): MqttError? {
        var result: MqttError? = null
        if (isOpen && isConnected) {
            val rc = MQTTClient_subscribe(handle = client.value, topic = topic, qos = qos.value)
            if (rc != MQTTCLIENT_SUCCESS) {
                result = MqttError("Cannot subscribe (code $rc).", MqttStatus.SUBSCRIBE_FAILED)
            }
        }
        return result
    }

    @Suppress("unused", "RedundantSuspendModifier")
    actual suspend fun unsubscribe(vararg topics: String): MqttError? {
        var result: MqttError? = null
        if (isOpen && isConnected) {
            topics.forEach { t ->
                if (MQTTClient_unsubscribe(client.value, t) != MQTTCLIENT_SUCCESS) {
                    result = MqttError("Cannot unsubscribe.", MqttStatus.UNSUBSCRIBE_FAILED)
                }
            }
        }
        return result
    }

    @Suppress("unused", "RedundantSuspendModifier")
    actual suspend fun connect(connOptions: MqttConnectionOptions): MqttError? = memScoped {
        var result: MqttError? = null
        if (isOpen && !isConnected) {
            updateConnOptions(newConnOptions = connOptions, newUsername = connOptions.username.cstr.ptr,
                newPassword = connOptions.password.cstr.ptr)
            val status = mqttConnectStatus(MQTTClient_connect(client.value, this@MqttClient.connOptionsPtr))
            if (status != MqttStatus.SUCCESS) result = MqttError("Cannot connect to MQTT Broker.", status)
        }
        result
    }

    private fun mqttConnectStatus(value: Int) = when (value) {
        1 -> MqttStatus.UNACCEPTABLE_PROTOCOL
        2 -> MqttStatus.IDENTIFIER_REJECTED
        3 -> MqttStatus.SERVER_UNAVAILABLE
        4 -> MqttStatus.INVALID_CREDENTIALS
        5 -> MqttStatus.NOT_AUTHORIZED
        else -> MqttStatus.SUCCESS
    }

    private fun updateConnOptions(newConnOptions: MqttConnectionOptions, newUsername: CPointer<ByteVar>?,
                                  newPassword: CPointer<ByteVar>?) {
        setupConnOptionsReturned()
        setupConnOptionsBinarypwd()
        if (connOptionsPtr != null) {
            with(connOptionsPtr.pointed) {
                username = newUsername
                password = newPassword
                keepAliveInterval = newConnOptions.keepAliveInterval
                cleansession = if (newConnOptions.cleanSession) true.toInt() else false.toInt()
                cleanstart = if (newConnOptions.cleanStart) true.toInt() else false.toInt()
                connectTimeout = newConnOptions.connectionTimeout
                retryInterval = newConnOptions.retryInterval
            }
        }
    }

    private fun setupConnOptionsBinarypwd() {
        if (connOptionsPtr != null) {
            with(connOptionsPtr.pointed.binarypwd) {
                len = 0
                data = null
            }
        }
    }

    private fun setupConnOptionsReturned() {
        if (connOptionsPtr != null) {
            with(connOptionsPtr.pointed.returned) {
                serverURI = null
                sessionPresent = 0
                MQTTVersion = 0
            }
        }
    }

    @Suppress("RedundantSuspendModifier")
    actual suspend fun disconnect(): MqttError? {
        var result: MqttError? = null
        if (isOpen && isConnected) {
            val rc = MQTTClient_disconnect(client.value, 2000)
            if (rc != MQTTCLIENT_SUCCESS) {
                result = MqttError("Cannot disconnect from MQTT Broker (code $rc).", MqttStatus.DISCONNECT_FAILED)
            }
        }
        return result
    }

    /** Disconnects from the MQTT Broker if needed, and frees up resources. */
    @Suppress("unused")
    suspend fun close() {
        if (isOpen) {
            if (isConnected) disconnect()
            MQTTClient_free(client.ptr)
            if (connOptionsPtr != null) free(connOptionsPtr)
            stableRef.dispose()
            _isOpen = false
        }
    }
}

private fun onDeliveryComplete(ctx: CPointer<*>?, deliveryToken: MQTTClient_deliveryToken) {
    initRuntimeIfNeeded()
    val ref = ctx?.asStableRef<MqttEventHandlers>()?.get()
    // Use a null check to ensure the lambda is called otherwise a compile error will occur.
    @Suppress("IfThenToSafeAccess")
    if (ref != null) ref.deliveryComplete(deliveryToken)
}

private fun onMessageArrived(
    ctx: CPointer<*>?,
    topicName: CPointer<ByteVar>?,
    @Suppress("UNUSED_PARAMETER") topicLength: Int,
    msg: CPointer<MQTTClient_message>?
): Int {
    initRuntimeIfNeeded()
    val tmpTopicName = topicName?.toKString() ?: ""
    val ref = ctx?.asStableRef<MqttEventHandlers>()?.get()
    // Use a null check to ensure the lambda is called otherwise a compile error will occur.
    @Suppress("IfThenToSafeAccess")
    if (ref != null) ref.messageArrived(tmpTopicName, createMessage(msg))
    if (topicName != null) MQTTClient_free(topicName)
    if (msg != null) MQTTClient_freeMessage(cValuesOf(msg))
    return 1
}

private fun onConnectionLost(ctx: CPointer<*>?, cause: CPointer<ByteVar>?) {
    initRuntimeIfNeeded()
    val ref = ctx?.asStableRef<MqttEventHandlers>()?.get()
    // Use a null check to ensure the lambda is called otherwise a compile error will occur.
    @Suppress("IfThenToSafeAccess")
    if (ref != null) ref.connectionLost(cause?.toKString() ?: "")
}

private fun createMessage(msg: CPointer<MQTTClient_message>?): MqttMessage {
    val payload = msg?.pointed?.payload?.reinterpret<ByteVar>()?.toKString() ?: ""
    val retained = msg?.pointed?.retained ?: false.toInt()
    val duplicate = msg?.pointed?.dup ?: false.toInt()
    return MqttMessage(
        msgId = msg?.pointed?.msgid ?: 0,
        payload = payload,
        qos = createMqttQos(msg?.pointed?.qos ?: 0),
        retained = retained == true.toInt(),
        duplicate = duplicate == true.toInt()
    )
}
