package org.digieng.kmqtt.client

/**
 * Exists as a workaround to deal with Kotlin Native's opinionated memory model, which is a **pain in the void**. The
 * memory model doesn't fit in with Kotlin's un-opinionated nature.
 */
internal data class MqttEventHandlers(
    val deliveryComplete: (Int) -> Unit = {},
    val connectionLost: (String) -> Unit = {},
    val messageArrived: (String, MqttMessage) -> Unit = { _, _ -> }
)
