package org.digieng.kmqtt.client

/** Returns the Int value of the Boolean. */
fun Boolean.toInt(): Int = if (this) 1 else 0
