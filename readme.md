# KMQTT Client (kmqtt-client)

A [MQTT](https://en.wikipedia.org/wiki/MQTT) client library for Kotlin projects using Kotlin Native, and/or Kotlin JVM. 
Kotlin *1.4.20* or greater is required for Kotlin projects using this library. The following targets are supported:

- **jvm**
- **linuxX64**


## Usage

All core MQTT functionality is covered by the MqttClient class which requires a MQTT Broker URL 
(eg **tcp://localhost:8883**) to be passed as a constructor parameter, eg:

```kotlin
val mqttClient = MqttClient(brokerUrl = "tcp://localhost:8883")
```

A MqttClient instance is created for each MQTT Broker. Note that the brokerUrl property is **read only**!

Publishing is done through the publish function which is non blocking, eg:

```kotlin
// ...
val msg = MqttMessage(payload = "on")
mqttClient.publish(topic = "/lights/1/state", msg = msg)
```

Subscribing is done through the subscribe function which is non blocking, eg:

```kotlin
// ...
mqttClient.subscribe(topic = "/lights/#")
```


### Kotlin JVM

Event handling is handled by the following MqttClient properties (each one only accepts a single event handler):

- **deliveryCompleteHandler**
- **connectionLostHandler**
- **messageArrivedHandler**


### Kotlin Native

Event handling is handled by the following MqttClient constructor arguments (each one only accepts a single event 
handler):

- **deliveryComplete**
- **connectionLost**
- **messageArrived**

Note that all event handlers **MUST** be top level functions, and they cannot capture state as per the Kotlin Native 
memory model requirements for callbacks (event handlers).

Remember to close the MqttClient instance when you are finished with it to free up unused 
resources (eg memory), eg:

```kotlin
// ...
mqttClient.close()
```
